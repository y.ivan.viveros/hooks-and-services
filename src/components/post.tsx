import React, { FC } from "react";
import {
  Button,
  NativeSyntheticEvent,
  NativeTouchEvent,
  StyleSheet,
  Text,
  View,
} from "react-native";

import { IPost } from "../hooks/use-posts";

export interface IPostProps {
  post: IPost;
  onDelete: (ev: NativeSyntheticEvent<NativeTouchEvent>) => void;
}

const Post: FC<IPostProps> = ({ post, onDelete }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title} numberOfLines={1}>
        {post.title}
      </Text>
      <Text numberOfLines={3}>{post.body}</Text>
      <Button title="Delete" onPress={onDelete} />
    </View>
  );
};

export default Post;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
  },
  title: {
    fontSize: 16,
    fontWeight: "500",
    marginBottom: 8,
    textTransform: "capitalize",
  },
});
