import axios, { AxiosResponse } from "axios";

import { IPost } from "../hooks/use-posts";

const endpoint = "/posts";

/**
 * ## Posts service
 *
 * Methods to make requests to the api to create/read/update/delete...
 */

class PostsService {
  public getAllPosts = async (): Promise<IPost[]> => {
    const { data } = await axios.get(endpoint);
    return data;
  };

  public deletePost = async (post: IPost): Promise<AxiosResponse<IPost>> => {
    return await axios.delete(`${endpoint}/${post.id}`);
  };

  public addPost = async (post: IPost): Promise<IPost> => {
    const { data } = await axios.post(endpoint, post);
    return data;
  };

  public editPost = (post: IPost): Promise<AxiosResponse<any>> => {
    return axios.put(`${endpoint}/${post.id}`, post);
  };
}

const postsService = new PostsService();

export default postsService;
