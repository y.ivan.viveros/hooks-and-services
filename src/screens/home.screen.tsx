import React, { FC } from "react";
import { FlatList, SafeAreaView, StyleSheet, Text, View } from "react-native";

import Post from "../components/post";
import usePosts from "../hooks/use-posts";

export interface IHomeScreenProps {}

const HomeScreen: FC<IHomeScreenProps> = ({}) => {
  const { posts, createPost, deletePost, editPost } = usePosts();

  return (
    <View>
      <SafeAreaView>
        <Text style={styles.title}>All Posts</Text>
      </SafeAreaView>
      <FlatList
        data={posts}
        keyExtractor={(post) => post.id.toString()}
        ItemSeparatorComponent={() => <View style={styles.separator} />}
        renderItem={({ item: post }) => (
          <Post post={post} onDelete={() => deletePost(post)} />
        )}
      />
    </View>
  );
};

export default HomeScreen;

const styles = StyleSheet.create({
  title: {
    fontSize: 20,
    fontWeight: "600",
    paddingHorizontal: 16,
    marginBottom: 22,
  },
  separator: {
    height: 1,
    backgroundColor: "#eee",
    marginVertical: 12,
  },
});
