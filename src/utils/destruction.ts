import { Alert } from "react-native";

const confirmDestruction = (itemName: string, onConfirm: () => void) => {
  Alert.alert("Warning", `Are you sure you want to delete ${itemName}?`, [
    {
      text: "Cancel",
      style: "cancel",
    },
    {
      text: "Delete",
      style: "destructive",
      onPress: onConfirm,
    },
  ]);
};

export default confirmDestruction;
