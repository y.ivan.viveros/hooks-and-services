import useSWR from "swr";
import { useEffect, useState } from "react";

import postsService from "../services/posts.service";
import confirmDestruction from "../utils/destruction";

export interface IPost {
  userId: number;
  id: number;
  title: string;
  body: string;
}

export const cacheKey = "posts";

/**
 * ## Posts complete CRUD
 *
 * Updates the global state (swr cache) and makes the
 * request to the backend to update the DB following
 * the optimistic UI pattern whenever possible.
 */

const usePosts = () => {
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const {
    data: posts,
    mutate,
    error: swrError,
    revalidate,
    isValidating,
  } = useSWR(cacheKey, postsService.getAllPosts);

  useEffect(() => {
    if (!swrError) return;
    setError(swrError);
  }, [swrError]);

  useEffect(() => {
    if (!isValidating) return;
    setLoading(true);
  }, [isValidating]);

  const createPost = async (post: IPost) => {
    try {
      setLoading(true);
      const newPost = await postsService.addPost(post);
      setLoading(false);
      mutate((posts) => [...posts, newPost], false);
    } catch (error) {
      setLoading(false);
      setError(error);
    }
  };

  const destroy = async (post: IPost) => {
    try {
      mutate((posts) => posts.filter((p) => p.id !== post.id), false);
      await postsService.deletePost(post);
    } catch (error) {
      setError(error);
    }
  };

  const deletePost = (post: IPost) => {
    confirmDestruction(post.title, () => destroy(post));
  };

  const editPost = async (post: IPost) => {
    try {
      mutate((posts) => posts.map((p) => (p.id === post.id ? post : p)), false);
      await postsService.editPost(post);
    } catch (error) {
      setError(error);
    }
  };

  return {
    posts,
    createPost,
    deletePost,
    editPost,
    error,
    loading,
    revalidate,
  };
};

export default usePosts;
