import React from "react";
import axios from "axios";

import HomeScreen from "./src/screens/home.screen";

axios.defaults.baseURL = "https://jsonplaceholder.typicode.com";

export default function App() {
  return <HomeScreen />;
}
